
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import apis from 'common/api.js'
import store from 'common/store.js'
Vue.prototype.$api = apis
Vue.prototype.$store = store
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif