import request from './http.js'
// 获取配置
export default {
	// 微信小程序
	// 登录
	login: (data, fn) => request.Post('/WxOpen/OnLogin', data, fn),
	//注册
	register: (data, fn) => request.Post('/WxOpen/OnRegister', data, fn),
	
	// 场馆管理
	//获取场馆列表
	getOrganization: (data, fn) => request.Get('/api/organization', data, fn),
	//获取场馆信息
	getOrganizationInfo: (param,data, fn) => request.Get('/api/organization/'+param, data, fn),
	//获取场地列表
	getPlace: (data, fn) => request.Get('/api/place/paged-list', data, fn),
	//获取场地信息
	getPlaceInfo: (param,data, fn) => request.Get('/api/place/'+param, data, fn),
	//获取体育列表
	getSport: (data, fn) => request.Get('/api/sport', data, fn),
	//获取体育信息
	getSportInfo: (param,data, fn) => request.Get('/api/sport/'+param, data, fn),
	
	
	// 排程管理
	//获取排程列表
	getPlaceSchedule: (data, fn) => request.Get('/api/place-schedule-with-date', data, fn),
	//获取排程列表，按场次、日期分组
	getPlaceScheduleBytime: (data, fn) => request.Get('/api/place-schedule-with-date/by-time', data, fn),
	//获取排程列表，按日期、场次分组
	getPlaceScheduleBydate: (data, fn) => request.Get('/api/place-schedule-with-date/by-date', data, fn),
	//获取排程信息
	getPlaceScheduleInfo: (param,data, fn) => request.Get('/api/place-schedule-with-date/'+param, data, fn),
	
	
	// 用户管理
	//获取余额信息
	getbalanceInfo: (data, fn) => request.Get('/api/user-balance/balance', data, fn),
	//获取账目列表
	getBalance: (data, fn) => request.Get('/api/user-balance-transaction/paged-list', data, fn),
	//获取账目信息
	getBalanceInfo: (param,data, fn) => request.Get('/api/user-balance-transaction/'+param, data, fn),
	//账目充值
	getRechargeBalance: (data, fn) => request.Post('/api/user-balance-transaction/recharge-create', data, fn),
	//记录取消充值账目
	cancelBalanceInfo: (param,data, fn) => request.Post(`/api/user-balance-transaction/${param}/recharge-cancel/`, data, fn),
	// 更新用户信息
	updataUserInfo: (data, fn) => request.Put('/api/user-external', data, fn),
	
	
	// 优惠券
	// 获取待领取的优惠券列表
	getCouponPoolWaring: (data, fn) => request.Get('/api/coupon-pool/paged-list', data, fn),
	// 获取充值优惠券列表
	getCouponPool: (data, fn) => request.Get('/api/user-balance-coupon-pool/paged-list', data, fn),
	// 获取充值优惠券
	getCouponPoolInfo: (param,data, fn) => request.Get('/api/user-balance-coupon-pool/'+param, data, fn),
	// 获取优惠券列表
	getCoupon: (data, fn) => request.Get('/api/user-coupon/paged-list', data, fn),
	// 领取优惠券
	toGetCoupon: (data, fn) => request.Post('/api/user-coupon/acquire', data, fn),
	
	
	//预约管理
	// 获取预约列表
	getBooking: (data, fn) => request.Get('/api/user-booking/paged-list', data, fn),
	// 获取预约信息
	getBookingInfo: (param,data, fn) => request.Get('/api/user-booking/'+param, data, fn),
	//删除预约信息
	deleteBookingInfo: (param,data, fn) => request.Delete('/api/user-booking/'+param, data, fn),
	//取消预约
	cancelBookingInfo: (param,data, fn) => request.Post('/api/user-booking/'+param+'/cancel', data, fn),
	// 提交预约
	confirmBooking: (data, fn) => request.Post('/api/user-booking', data, fn),
	
	// 上传图片
	uploadImg: (data, fn) => request.Post('/api/image/upload', data, fn),
	
	//小程序使用微信内部支付 
	JsApiWxOpen: (data, fn) => request.Post('/TenPayRealV3/JsApiWxOpen', data, fn),
	
	//微信公众号使用微信内部支付 
	JsApi: (data, fn) => request.Post('/TenPayRealV3/JsApi', data, fn),
	// 获取openid
	getOpenid: (data, fn) => request.Post('/TenPayRealV3/snsapi_base', data, fn),
	
	// getCode: (data, fn) => request.Post('/get_code', data, fn),

}
