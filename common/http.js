import store from './store'
import md5 from './md5'

const Toast = (msg, type = 'error', time = 2000) => {
	wx.showToast({
		title: msg,
		icon: 'none',
		duration: time
	})
}

const BASE_URL = 'https://www.xjmyyxt.cn';
// const BASE_URL = "/api"
const PREFIX = 'TUTTI_';
const SIGN_KEY = 'HUBYF^r$%vr7$E';

function request(method, url, params = {}, callback = () => {}) {
	let handler = {}
	handler.url = BASE_URL + url
	handler.data = params
	handler.header = {
		'content-type': 'application/json; charset=UTF-8'
	}
	handler.method = method

	let Cookie = store.Get('Cookie')
	let token = store.Get('token') || ''
	if (!token && token === 'undefined') {
		token = ''
	}
	let timestamp = Date.parse(new Date().toString()).toString().substr(0, 10)
	let nonce = Math.round(Math.random() * 1000000000);
	let device = store.Get('device');
	let os = "3";
	let appid = "101";
	let version = "1.01";
	let body = JSON.stringify(params);
	let str = PREFIX + device + os + appid + version + timestamp + nonce + SIGN_KEY + body + token;
	let sign = md5(str)

	handler.header['Client-Debug'] = 1
	handler.header['Client-Timestamp'] = timestamp
	handler.header['Client-Nonce'] = nonce
	handler.header['Client-Device'] = device
	handler.header['Client-Os'] = os
	handler.header['Client-AppId'] = appid
	handler.header['Client-Version'] = version
	handler.header['Client-Sign'] = sign
	handler.header['Client-Token'] = token
	handler.header['Authorization'] = 'Bearer ' + uni.getStorageSync('access_token')
	// handler.header['Cookie'] = Cookie

	handler.success = res => {
		// if (res.header['Set-Cookie']){
		// 	console.log('-----kkk--1111-', res.header['Set-Cookie'])
		// 	console.log('-----kkk--1111-', res.header['Set-Cookie'].split(";"))
		// 	let str = res.header['Set-Cookie'].split(";")[0]
		// 	console.log('-----kkk---', str)
		// 	store.Set('Cookie',str);
		// }

		// console.log('-----kkk---', str.split("=")[1])
		if (res.data.status === 0) {
			callback(res.data)
		} else {
			callback(res.data)
		}
	}
	handler.fail = err => {
		wx.showModal({
			title: '网络错误',
			content: JSON.stringify(err),
			showCancel: false
		})
		callback(err)
	}
	wx.request(handler)
}

const Get = (url, params = {}, handler = {}) => {
	return request('GET', url, params, handler)
}
const Post = (url, params = {}, handler = {}) => {
	return request('POST', url, params, handler)
}
const Delete = (url, params = {}, handler = {}) => {
	return request('DELETE', url, params, handler)
}
const Put = (url, params = {}, handler = {}) => {
	return request('PUT', url, params, handler)
}
module.exports = {
	Post,
	Get,
	Delete,
	Put
}
